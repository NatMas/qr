import { Component } from '@angular/core';
import { DataLocalService } from '../../services/data-local.service';
import { Registro } from '../../models/registro.model';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  registros: Registro[];

  constructor(public dataLocal : DataLocalService) {
    this.dataLocal.cargarStorage()
    this.registros = this.dataLocal.guardados
  }
  ngOnInit():void{
    this.dataLocal.cargarStorage();
    this.registros=this.dataLocal.guardados;
  }

  abrirRegistro(registro){
    console.log(registro+(" aaaaaaaaaaaaaa"))
    this.dataLocal.abrirRegistro(registro);
  }

  enviarCorreo(){
    console.log("Enviar correeeeeeeeeo :D")
    this.dataLocal.enviarCorreo();
  }

  

}
